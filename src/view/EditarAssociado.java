package view;

import java.awt.Color;
import java.awt.Toolkit;
import java.text.ParseException;

import javax.swing.ButtonGroup;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.MaskFormatter;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EditarAssociado extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel 					contentPane;
	private final ButtonGroup 		buttonGroup = new ButtonGroup();	
	private JTextField 				txtNome;
	private JTextField 				txtProntuario;
	private JTextField 				txtEmail;
	private JRadioButton 			rbServidor;
	private JRadioButton 			rbAluno;
	private JRadioButton 			rbProfessor;
	private JPanel 					panelDadosPessoais;
	private JLabel 					lblCPF;
	private JLabel 					lblData_Nascimento;
	private JTextField 				txtEndereco;
	private JTextField 				txtLogradouro;
	private JTextField 				txtCidade;
	private JFormattedTextField 	txtCEP;
	private JTextField 				txtEstado;
	private JFormattedTextField 	txtNumero;
	private JFormattedTextField 	txtCPF;
	private JFormattedTextField 	txtRG;
	private JFormattedTextField 	txtTelefone;
	private JFormattedTextField 	txtData;
	private MaskFormatter			maskCPF;
	private MaskFormatter			maskRG;
	private MaskFormatter			maskTelefone;
	private MaskFormatter			maskData;
	private JButton btnSalvar;
	private JButton btnCancelar;
	
	public EditarAssociado(String nome, String prontuario, String email, String associado, String cpf, String rg, String telefone, String data, String endereco, String logradouro, String cidade, String cep, String estado, String numero, Boolean exib) {
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(TelaCadastro.class.getResource("/img/icon_usuario.png")));
		setResizable(false);
		setTitle("Editar de Associado - Biblioteca");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 400, 600);
		setLocationRelativeTo(null);	
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);	
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Informa\u00E7\u00F5es B\u00E1sicas ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 395, 220);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(10, 20, 100, 20);
		panel.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setColumns(10);
		txtNome.setBounds(125, 20, 250, 20);
		panel.add(txtNome);
		
		JLabel lblProntuario = new JLabel("Prontu�rio:");
		lblProntuario.setBounds(10, 60, 100, 20);
		panel.add(lblProntuario);
		
		txtProntuario = new JTextField();
		txtProntuario.setColumns(10);
		txtProntuario.setBounds(125, 60, 180, 20);
		panel.add(txtProntuario);
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setBounds(10, 91, 100, 20);
		panel.add(lblEmail);

				
		JLabel lblTipo = new JLabel("Tipo Associado:");
		lblTipo.setBounds(10, 118, 100, 14);
		panel.add(lblTipo);
		
		rbProfessor = new JRadioButton("Professor");
		buttonGroup.add(rbProfessor);
		rbProfessor.setBackground(Color.WHITE);
		rbProfessor.setBounds(48, 139, 100, 25);
		panel.add(rbProfessor);
		
		rbAluno = new JRadioButton("Aluno");
		buttonGroup.add(rbAluno);
		rbAluno.setBackground(Color.WHITE);
		rbAluno.setBounds(48, 164, 100, 25);
		panel.add(rbAluno);
		
		rbServidor = new JRadioButton("Funcion�rio");
		buttonGroup.add(rbServidor);
		rbServidor.setBackground(Color.WHITE);
		rbServidor.setBounds(48, 189, 100, 25);
		panel.add(rbServidor);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(125, 91, 250, 20);
		panel.add(txtEmail);

		try {
			maskCPF = new MaskFormatter("###.###.###-##");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		try {
			maskRG = new MaskFormatter("##.###.###-A");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		try {
			maskTelefone = new MaskFormatter("(##) #####-####");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		

		try {
			maskData = new MaskFormatter("####-##-##");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		JPanel panelEndereco = new JPanel();
		panelEndereco.setLayout(null);
		panelEndereco.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Enderen�o ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelEndereco.setBackground(Color.WHITE);
		panelEndereco.setBounds(0, 370, 395, 140);
		contentPane.add(panelEndereco);
		
		JLabel lblEndereco = new JLabel("Endere�o:");
		lblEndereco.setBounds(10, 25, 100, 20);
		panelEndereco.add(lblEndereco);
		
		JLabel lblLogradouro = new JLabel("Logradouro:");
		lblLogradouro.setBounds(10, 50, 100, 20);
		panelEndereco.add(lblLogradouro);
		
		JLabel lblCidade = new JLabel("Cidade:");
		lblCidade.setBounds(10, 75, 100, 20);
		panelEndereco.add(lblCidade);
		
		JLabel lblEstado = new JLabel("Estado:");
		lblEstado.setBounds(10, 100, 100, 20);
		panelEndereco.add(lblEstado);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(120, 25, 265, 20);
		panelEndereco.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		txtLogradouro = new JTextField();
		txtLogradouro.setColumns(10);
		txtLogradouro.setBounds(120, 50, 60, 20);
		panelEndereco.add(txtLogradouro);
		
		txtCidade = new JTextField();
		txtCidade.setColumns(10);
		txtCidade.setBounds(120, 75, 100, 20);
		panelEndereco.add(txtCidade);
		
		JLabel lblCEP = new JLabel("CEP:");
		lblCEP.setBounds(230, 75, 60, 20);
		panelEndereco.add(lblCEP);
		
		txtCEP = new JFormattedTextField();
		txtCEP.setColumns(10);
		txtCEP.setBounds(300, 75, 85, 20);
		panelEndereco.add(txtCEP);
		
		txtEstado = new JTextField();
		txtEstado.setBounds(120, 100, 100, 20);
		panelEndereco.add(txtEstado);
		txtEstado.setColumns(10);
		
		JLabel lblNumero = new JLabel("N�mero:");
		lblNumero.setBounds(230, 103, 60, 20);
		panelEndereco.add(lblNumero);

		txtNumero = new JFormattedTextField();
		txtNumero.setColumns(10);
		txtNumero.setBounds(300, 100, 50, 20);
		panelEndereco.add(txtNumero);	
		
		
		
		panelDadosPessoais = new JPanel();
		panelDadosPessoais.setBounds(0, 219, 395, 150);
		contentPane.add(panelDadosPessoais);
		panelDadosPessoais.setBackground(Color.WHITE);
		panelDadosPessoais.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Dados Pessoais ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelDadosPessoais.setLayout(null);
		
		lblCPF = new JLabel("CPF:");
		lblCPF.setBounds(10, 25, 80, 20);
		panelDadosPessoais.add(lblCPF);
		
		JLabel lblRG = new JLabel("RG:");
		lblRG.setBounds(10, 50, 80, 20);
		panelDadosPessoais.add(lblRG);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setBounds(10, 75, 80, 20);
		panelDadosPessoais.add(lblTelefone);
		
		lblData_Nascimento = new JLabel("Nascimento:");
		lblData_Nascimento.setBounds(10, 100, 80, 20);
		panelDadosPessoais.add(lblData_Nascimento);
		txtCPF = new JFormattedTextField(maskCPF);
		
		txtCPF.setColumns(10);
		txtCPF.setBounds(120, 25, 265, 20);
		panelDadosPessoais.add(txtCPF);
		txtRG = new JFormattedTextField(maskRG);
		
		txtRG.setColumns(10);
		txtRG.setBounds(120, 50, 265, 20);
		panelDadosPessoais.add(txtRG);
		txtTelefone = new JFormattedTextField(maskTelefone);
		
		txtTelefone.setColumns(10);
		txtTelefone.setBounds(120, 75, 150, 20);
		panelDadosPessoais.add(txtTelefone);
		txtData = new JFormattedTextField(maskData);
		
		txtData.setColumns(10);
		txtData.setBounds(120, 100, 150, 20);
		panelDadosPessoais.add(txtData);		
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//String dia = data.substring(0,2);
				//String mes = data.substring(3,5);
				//String ano = data.substring(6,10);


			}
		});
		btnSalvar.setBounds(295, 515, 89, 23);
		contentPane.add(btnSalvar);
		
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(195, 515, 89, 23);
		contentPane.add(btnCancelar);
		
		
		Preencher(nome, prontuario, email, associado, cpf, rg, telefone, data, endereco, logradouro, cidade, cep, estado, numero);
		
		//contentPane.add(Associado(associado));
		setVisible(true);
	}
	
	
	public void Preencher(String nome, String prontuario, String email, String associado, String cpf, String rg, String telefone, String data, String endereco, String logradouro, String cidade, String cep, String estado, String numero) {
		txtNome.setText(nome);
		txtProntuario.setText(prontuario);
		txtEmail.setText(email);
		if(associado.equals("1")) {
			rbAluno.setSelected(true);
		}else if(associado.equals("2")) {
			rbProfessor.setSelected(true);
		}else if(associado.equals("3")) {
			rbServidor.setSelected(true);
		}
		
		txtCPF.setText(cpf);
		txtRG.setText(rg);
		txtTelefone.setText(telefone);
		txtData.setText(data);
		txtEndereco.setText(endereco);
		txtLogradouro.setText(logradouro);
		txtCidade.setText(cidade);
		txtCEP.setText(cep);
		txtEstado.setText(estado);
		txtNumero.setText(numero);
	}
	
	protected boolean CriarClass(String nome, String prontuario, String email, String associado, String cpf, String rg, String telefone, String dia, String mes, String ano, String endereco, String logradouro, String cidade, String cep, String estado, String numero) {
		return true;

	}
}
