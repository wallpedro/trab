package view;

import java.awt.Color;
import java.beans.PropertyVetoException;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.border.BevelBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

public class ExibirEmprestimo extends JInternalFrame {


	private static final long serialVersionUID = 1L;
	private JTextField textField;
	private JTable table;

	public ExibirEmprestimo() {
		setForeground(Color.WHITE);
		try {
			setIcon(true);
		} catch (PropertyVetoException e1) {
			e1.printStackTrace();
		}
		getContentPane().setBackground(Color.WHITE);
		
		JPanel JPanelEmprestimo = new JPanel();
		JPanelEmprestimo.setBounds(10, 10, 465, 350);
		JPanelEmprestimo.setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Empr\u00E9stimo ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		JPanelEmprestimo.setBackground(Color.WHITE);
		
		JLabel lblProntuario = new JLabel("Prontuário:");
		lblProntuario.setBounds(10, 35, 70, 20);
		
		textField = new JTextField();
		textField.setBounds(90, 35, 250, 20);
		textField.setColumns(10);
		
		JButton btnProntuario = new JButton("Buscar");
		btnProntuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ExibirAssociado();
			}
		});
		btnProntuario.setBounds(350, 35, 90, 20);
		
		JPanel JPanelAssociado = new JPanel();
		JPanelAssociado.setBounds(10, 65, 445, 110);
		JPanelAssociado.setBackground(Color.WHITE);
		
		JButton btnCancelar = new JButton("Fechar");
		btnCancelar.setBounds(394, 321, 65, 23);
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		getContentPane().setLayout(null);
		getContentPane().add(JPanelEmprestimo);
		JPanelEmprestimo.setLayout(null);
		JPanelEmprestimo.add(lblProntuario);
		JPanelEmprestimo.add(textField);
		JPanelEmprestimo.add(btnProntuario);
		JPanelEmprestimo.add(JPanelAssociado);
		JPanelAssociado.setLayout(null);
		
		table = new JTable();
		table.setBounds(10, 10, 425, 88);
		JPanelAssociado.add(table);
		JPanelEmprestimo.add(btnCancelar);
		setTitle("Tabela de Empréstimo");
		setBounds(200, 200, 500, 400);			
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		setVisible(true);
	}
}
