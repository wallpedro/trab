package view;


import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import Entidades.Administrador;



public class TelaLogin extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//private JDesktopPane desktopPane = new JDesktopPane();
	
	private JTextField  	txtUsuario;
	private JPasswordField 	txtSenha;
	private JButton 		btnEntrar;
	private JLabel 			lblErro;
	

	
	
	public TelaLogin() {

		
		setTitle("Biblioteca - Instituto Federal de S�o Paulo - Campus Guarulhos");
		setFont(new Font("Arial", Font.BOLD, 15));
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(TelaLogin.class.getResource("/img/imgBiblioteca.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		setResizable(false);		
		getContentPane().setLayout(null);		
		getContentPane().setBackground(Color.WHITE);
		setBackground(Color.BLACK);
		setFont(new Font("Arial", Font.BOLD, 15));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600, 300);
		setLocationRelativeTo(null);

		JPanel panel_Central = new JPanel();
		panel_Central.setBackground(Color.WHITE);
		panel_Central.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, null, null, null));
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(panel_Central, GroupLayout.PREFERRED_SIZE, 594, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(0, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addComponent(panel_Central, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
		);
		
		JPanel panel_login = new JPanel();
		panel_login.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Biblioteca IFSP - Ar\u00E9a do Usu\u00E1rio ", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_login.setBackground(Color.WHITE);
		
		JLabel lblUsuario = new JLabel("Usu�rio:");		
		lblUsuario.setBounds(15, 50, 60, 20);
		txtUsuario = new JTextField();
		txtUsuario.setBounds(80, 50, 180, 20);
		txtUsuario.setColumns(10);
		
		JLabel lblSenha = new JLabel("Senha:");		
		lblSenha.setBounds(15, 90, 60, 20);
		txtSenha = new JPasswordField();		
		txtSenha.setBounds(80, 90, 180, 20);
		btnEntrar = new JButton("Entrar");
		btnEntrar.setBounds(160, 150, 100, 30);
		btnEntrar.setBackground(Color.WHITE);
		btnEntrar.addActionListener(new ActionListener() {		
			public void actionPerformed(ActionEvent e) {
				String senha = new String(txtSenha.getPassword());
				Administrador usuario = new Administrador(txtUsuario.getText(), senha);
				if(usuario.ConfirmLogin()) {
					new Tela_Inicial();					
				}else {
					lblErro.setVisible(true);
				}
			}
		});
		
		lblErro = new JLabel("Voc� digitou usu�rio e/ou senha inv�lido!");
		lblErro.setVisible(false);
		lblErro.setBounds(0, 124, 276, 15);
		lblErro.setHorizontalAlignment(SwingConstants.CENTER);
		lblErro.setForeground(Color.RED);
		
		JLabel lbl_Imagem = new JLabel("");
		lbl_Imagem.setIcon(new ImageIcon(TelaLogin.class.getResource("/img/logo_ifsp.jpg")));
		GroupLayout gl_panel_Central = new GroupLayout(panel_Central);
		gl_panel_Central.setHorizontalGroup(
			gl_panel_Central.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_Central.createSequentialGroup()
					.addGap(63)
					.addComponent(lbl_Imagem, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(18)
					.addComponent(panel_login, GroupLayout.PREFERRED_SIZE, 276, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_panel_Central.setVerticalGroup(
			gl_panel_Central.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_Central.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_panel_Central.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel_login, GroupLayout.PREFERRED_SIZE, 247, GroupLayout.PREFERRED_SIZE)
						.addComponent(lbl_Imagem))
					.addContainerGap())
		);
		panel_login.setLayout(null);
		panel_login.add(btnEntrar);
		panel_login.add(lblUsuario);
		panel_login.add(lblSenha);
		panel_login.add(txtSenha);
		panel_login.add(txtUsuario);
		panel_login.add(lblErro);
		panel_Central.setLayout(gl_panel_Central);
		getContentPane().setLayout(groupLayout);
		setVisible(true);
	}
	

}
