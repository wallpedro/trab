package view;

import java.awt.Color;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.text.MaskFormatter;
import javax.swing.border.BevelBorder;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;

import java.awt.event.ActionListener;
import java.text.ParseException;
import java.awt.event.ActionEvent;

public class TelaCadastro2 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel 					contentPane;
	private JPanel 					panelDadosPessoais;
	private JLabel 					lblCPF;
	private JLabel 					lblData_Nascimento;
	private JTextField 				txtEndereco;
	private JTextField 				txtLogradouro;
	private JTextField 				txtCidade;
	private JFormattedTextField 	txtCEP;
	private JTextField 				txtEstado;
	private JFormattedTextField 	txtNumero;
	private JFormattedTextField 	txtCPF;
	private JFormattedTextField 	txtRG;
	private JFormattedTextField 	txtTelefone;
	private JFormattedTextField 	txtDia;
	private JFormattedTextField 	txtMes;
	private JFormattedTextField 	txtAno;
	private JTextField 				txtTipoAssociado;
	private JLabel 					lblCargo;
	private JFormattedTextField 	txtComplemento;	
	private JLabel					lblFormacao;
	private JLabel 					lblModulo;
	
	
	private MaskFormatter			maskCPF;
	private MaskFormatter			maskRG;
	private MaskFormatter			maskTelefone;
	private MaskFormatter			maskDia;
	private MaskFormatter			maskMes;
	private MaskFormatter			maskAno;
	private MaskFormatter			maskComplemento;
	




	public TelaCadastro2(String nome, String prontuario, String email, String associado) {		
		setTitle("Cadastro de Associado - Biblioteca");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 400, 500);
		
		setResizable(false);
		setLocationRelativeTo(null);
		setIconImage(Toolkit.getDefaultToolkit().getImage(TelaCadastro2.class.getResource("/img/icon_usuario.png")));
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);		
		getContentPane().setBackground(Color.WHITE);

		
		panelDadosPessoais = new JPanel();
		panelDadosPessoais.setBackground(Color.WHITE);
		panelDadosPessoais.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Dados Pessoais ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelDadosPessoais.setBounds(0, 0, 395, 150);
		contentPane.add(panelDadosPessoais);	
		panelDadosPessoais.setLayout(null);
		
		lblCPF = new JLabel("CPF:");
		lblCPF.setBounds(10, 25, 80, 20);
		panelDadosPessoais.add(lblCPF);
		
		JLabel lblRG = new JLabel("RG:");
		lblRG.setBounds(10, 50, 80, 20);
		panelDadosPessoais.add(lblRG);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setBounds(10, 75, 80, 20);
		panelDadosPessoais.add(lblTelefone);
		
		lblData_Nascimento = new JLabel("Nascimento:");
		lblData_Nascimento.setBounds(10, 100, 80, 20);
		panelDadosPessoais.add(lblData_Nascimento);
		
		try {
			maskCPF = new MaskFormatter("###.###.###-##");
			txtCPF = new JFormattedTextField(maskCPF);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		txtCPF.setColumns(10);
		txtCPF.setBounds(120, 25, 265, 20);
		panelDadosPessoais.add(txtCPF);
		
		try {
			maskRG = new MaskFormatter("##.###.###-A");
			txtRG = new JFormattedTextField(maskRG);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		txtRG.setColumns(10);
		txtRG.setBounds(120, 50, 265, 20);
		panelDadosPessoais.add(txtRG);
		
		try {
			maskTelefone = new MaskFormatter("(##) #####-####");
			txtTelefone = new JFormattedTextField(maskTelefone);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		txtTelefone.setColumns(10);
		txtTelefone.setBounds(120, 75, 150, 20);
		panelDadosPessoais.add(txtTelefone);
		
		try {
			maskDia = new MaskFormatter("##");
			txtDia = new JFormattedTextField(maskDia);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		txtDia.setColumns(10);
		txtDia.setBounds(150, 100, 40, 20);
		panelDadosPessoais.add(txtDia);
		
		JLabel lblDia = new JLabel("Dia:");
		lblDia.setBounds(120, 100, 30, 20);
		panelDadosPessoais.add(lblDia);
		
		JLabel lblMes = new JLabel("M�s:");
		lblMes.setBounds(200, 100, 30, 20);
		panelDadosPessoais.add(lblMes);
		
		try {
			maskMes = new MaskFormatter("##");
			txtMes = new JFormattedTextField(maskMes);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		txtMes.setColumns(10);
		txtMes.setBounds(235, 100, 40, 20);
		panelDadosPessoais.add(txtMes);
		
		JLabel lblAno = new JLabel("Ano:");
		lblAno.setBounds(280, 100, 30, 20);
		panelDadosPessoais.add(lblAno);
		
		try {
			maskAno = new MaskFormatter("####");
			txtAno = new JFormattedTextField(maskAno);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		txtAno.setColumns(10);
		txtAno.setBounds(320, 100, 40, 20);
		panelDadosPessoais.add(txtAno);
		
		JPanel panelEndereco = new JPanel();
		panelEndereco.setLayout(null);
		panelEndereco.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Enderen\u00E7o ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelEndereco.setBackground(Color.WHITE);
		panelEndereco.setBounds(0, 150, 395, 140);
		contentPane.add(panelEndereco);
		
		JLabel lblEndereco = new JLabel("Endere�o:");
		lblEndereco.setBounds(10, 25, 100, 20);
		panelEndereco.add(lblEndereco);
		
		JLabel lblLogradouro = new JLabel("Logradouro:");
		lblLogradouro.setBounds(10, 50, 100, 20);
		panelEndereco.add(lblLogradouro);
		
		JLabel lblCidade = new JLabel("Cidade:");
		lblCidade.setBounds(10, 75, 100, 20);
		panelEndereco.add(lblCidade);
		
		JLabel lblEstado = new JLabel("Estado:");
		lblEstado.setBounds(10, 100, 100, 20);
		panelEndereco.add(lblEstado);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(120, 25, 265, 20);
		panelEndereco.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		txtLogradouro = new JTextField();
		txtLogradouro.setColumns(10);
		txtLogradouro.setBounds(120, 50, 60, 20);
		panelEndereco.add(txtLogradouro);
		
		txtCidade = new JTextField();
		txtCidade.setColumns(10);
		txtCidade.setBounds(120, 75, 100, 20);
		panelEndereco.add(txtCidade);
		
		JLabel lblCEP = new JLabel("CEP:");
		lblCEP.setBounds(230, 75, 60, 20);
		panelEndereco.add(lblCEP);
		
		txtCEP = new JFormattedTextField();
		txtCEP.setColumns(10);
		txtCEP.setBounds(300, 75, 85, 20);
		panelEndereco.add(txtCEP);
		
		txtEstado = new JTextField();
		txtEstado.setBounds(120, 100, 100, 20);
		panelEndereco.add(txtEstado);
		txtEstado.setColumns(10);
		
		JLabel lblNumero = new JLabel("N�mero:");
		lblNumero.setBounds(230, 103, 60, 20);
		panelEndereco.add(lblNumero);

		txtNumero = new JFormattedTextField();
		txtNumero.setColumns(10);
		txtNumero.setBounds(300, 100, 50, 20);
		panelEndereco.add(txtNumero);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new TelaCadastro().Voltar(nome, prontuario, email, associado);
				setVisible(false);
				
			}
		});
		btnVoltar.setBounds(174, 435, 100, 25);
		contentPane.add(btnVoltar);
		
		JButton btnProximo = new JButton("Pr�ximo");
		btnProximo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				Proximo(nome, prontuario, email, associado);
			}
		});
		btnProximo.setBounds(285, 435, 100, 25);
		contentPane.add(btnProximo);
		contentPane.add(Associado(associado));
		setVisible(true);
	}
	
	
	
	public JPanel Associado(String associado) {			
		JPanel JPanelAssociado = new JPanel();		
		if(associado.equals("Aluno")) {			
			
			JPanelAssociado.setLayout(null);
			JPanelAssociado.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Tipo de Associado " + associado, TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			JPanelAssociado.setBackground(Color.WHITE);
			JPanelAssociado.setBounds(0, 295, 395, 140);
			contentPane.add(JPanelAssociado);
			
			JLabel lblTipoAssociado = new JLabel("Tipo Associado:");
			lblTipoAssociado.setBounds(10, 25, 100, 20);
			JPanelAssociado.add(lblTipoAssociado);
			
			txtTipoAssociado = new JTextField();
			txtTipoAssociado.setEnabled(false);
			txtTipoAssociado.setColumns(10);
			txtTipoAssociado.setBounds(120, 25, 250, 20);
			JPanelAssociado.add(txtTipoAssociado);
			
			lblModulo = new JLabel("M�dulo:");
			lblModulo.setBounds(10, 50, 100, 20);
			JPanelAssociado.add(lblModulo);
			
			try {
				maskComplemento = new MaskFormatter("##");
				txtComplemento = new JFormattedTextField(maskComplemento);	
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
			txtComplemento.setColumns(10);
			txtComplemento.setBounds(120, 50, 250, 20);
			JPanelAssociado.add(txtComplemento);
			
		}else if(associado.equals("Professor")) {
			
			JPanelAssociado.setLayout(null);
			JPanelAssociado.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Tipo de Associado " + associado, TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			JPanelAssociado.setBackground(Color.WHITE);
			JPanelAssociado.setBounds(0, 295, 395, 120);
			contentPane.add(JPanelAssociado);
			
			JLabel lblTipoAssociado = new JLabel("Tipo Associado");
			lblTipoAssociado.setBounds(10, 25, 100, 20);
			JPanelAssociado.add(lblTipoAssociado);
			
			txtTipoAssociado = new JTextField();
			txtTipoAssociado.setEnabled(false);
			txtTipoAssociado.setColumns(10);
			txtTipoAssociado.setBounds(120, 25, 250, 20);
			JPanelAssociado.add(txtTipoAssociado);
			
			lblFormacao = new JLabel("Formac�o:");
			lblFormacao.setBounds(10, 50, 100, 20);
			JPanelAssociado.add(lblFormacao);
			
			txtComplemento = new JFormattedTextField();			
			txtComplemento.setColumns(10);
			txtComplemento.setBounds(120, 50, 250, 20);
			JPanelAssociado.add(txtComplemento);		

			
		}else if(associado.equals("Funcion�rio")) {			
			
			JPanelAssociado.setLayout(null);
			JPanelAssociado.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Tipo de Associado " + associado, TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			JPanelAssociado.setBackground(Color.WHITE);
			JPanelAssociado.setBounds(0, 295, 395, 120);
			contentPane.add(JPanelAssociado);
			
			JLabel lblTipoAssociado = new JLabel("Tipo Associado:");
			lblTipoAssociado.setBounds(10, 25, 100, 20);
			JPanelAssociado.add(lblTipoAssociado);
			
			txtTipoAssociado = new JTextField();
			txtTipoAssociado.setEnabled(false);
			txtTipoAssociado.setColumns(10);
			txtTipoAssociado.setBounds(120, 25, 250, 20);
			JPanelAssociado.add(txtTipoAssociado);
			
			lblCargo = new JLabel("Cargo:");
			lblCargo.setBounds(10, 50, 100, 20);
			JPanelAssociado.add(lblCargo);
			
			txtComplemento = new JFormattedTextField();			
			txtComplemento.setColumns(10);
			txtComplemento.setBounds(120, 50, 250, 20);
			JPanelAssociado.add(txtComplemento);			
		}
		txtTipoAssociado.setText(associado);
		return JPanelAssociado;				
	}

	
	public void Proximo(String nome, String  prontuario, String  email, String associado) {	
		if(txtCPF.getText().isEmpty() || txtRG.getText().isEmpty() || txtTelefone.getText().isEmpty() || txtDia.getText().isEmpty() || txtMes.getText().isEmpty() || txtAno.getText().isEmpty() || txtEndereco.getText().isEmpty() || txtLogradouro.getText().isEmpty() || txtCidade.getText().isEmpty() || txtCEP.getText().isEmpty() || txtEstado.getText().isEmpty() || txtNumero.getText().isEmpty() || txtTipoAssociado.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Informe todos os campos", "Error", JOptionPane.ERROR_MESSAGE);
		}else {
			new TelaConfirm(nome, prontuario, email, associado, txtCPF.getText(), txtRG.getText(), txtTelefone.getText(), txtDia.getText(), txtMes.getText(), txtAno.getText(), txtEndereco.getText(), txtLogradouro.getText(), txtCidade.getText(), txtCEP.getText(), txtEstado.getText(), txtNumero.getText(), txtComplemento.getText(), false);		
			dispose();				
		}
		
	}
	
	public void Voltar(String cpf, String rg, String telefone, String dia, String mes, String ano, String endereco, String logradouro, String cidade, String cep, String estado, String numero, String complemento) {		
		this.txtCPF.setText(cpf);
		this.txtRG.setText(rg);
		this.txtTelefone.setText(telefone);
		this.txtDia.setText(dia);
		this.txtMes.setText(mes);
		this.txtAno.setText(ano);
		this.txtEndereco.setText(endereco);
		this.txtLogradouro.setText(logradouro);
		this.txtCidade.setText(cidade);
		this.txtCEP.setText(cep);
		this.txtEstado.setText(estado);
		this.txtNumero.setText(numero);
		this.txtComplemento.setText(complemento);
		
	}
}
