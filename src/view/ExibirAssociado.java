 package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import Entidades.Aluno;
import javax.swing.ButtonGroup;
import javax.swing.border.LineBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;


public class ExibirAssociado extends JInternalFrame {
	
	private static final long serialVersionUID = 1L;

	private JPanel 			jPanelPesquisa;
	private JButton			btnBuscar;
	private JTable 			jTableUsuario;
	private JScrollPane		scrollPane;
	private JPanel			jPanelTabela;
	private JLabel 			lblBuscar;
	private JTextField 		txtBuscar;

	private JRadioButton 	rdBAluno;
	private JRadioButton 	rdBProfessor;
	private JRadioButton	rdBFuncionario;
	
	private JPopupMenu 		menuJPopUp;
	private final ButtonGroup rdBGroup = new ButtonGroup();


	public ExibirAssociado() {
		setForeground(Color.WHITE);
		try {
			setIcon(true);
		} catch (PropertyVetoException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		getContentPane().setBackground(Color.WHITE);
		setTitle("Tabela de Associado");
		setBounds(200, 100, 1000, 500);		
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);				
		
		jPanelPesquisa = new JPanel();
		jPanelPesquisa.setBorder(new LineBorder(new Color(0, 0, 0)));
		jPanelPesquisa.setBackground(Color.WHITE);
		jPanelPesquisa.setBounds(5, 15, 975, 60);
		jPanelPesquisa.setLayout(null);
		
		jPanelTabela 	= new JPanel();
		jPanelTabela.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		jPanelTabela.setBackground(Color.WHITE);
		jPanelTabela.setBounds(5, 110, 975, 322);		
		
		
		//Label Buscar
		lblBuscar = new JLabel("Buscar:");
		lblBuscar.setFont(new Font("Arial", Font.PLAIN, 12));
		lblBuscar.setBounds(10,15,45,30);
		//Campo texto Buscar
		txtBuscar = new JTextField();
		txtBuscar.setFont(new Font("Arial", Font.PLAIN, 12));
		txtBuscar.setBounds(60, 15, 795, 30);
		//Bot�o Buscar
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refresh();
			}
		});
		btnBuscar.setFont(new Font("Arial", Font.BOLD, 12));
		btnBuscar.setBounds(865, 15, 100, 30);		
		
		jPanelPesquisa.add(lblBuscar);
		jPanelPesquisa.add(txtBuscar);
		jPanelPesquisa.add(btnBuscar);
		
		getContentPane().setLayout(null);		
		getContentPane().add(jPanelPesquisa);
		getContentPane().add(jPanelTabela);
		
		rdBAluno = new JRadioButton("Aluno");
		rdBGroup.add(rdBAluno);
		rdBAluno.setBackground(Color.WHITE);
		rdBAluno.setHorizontalAlignment(SwingConstants.CENTER);
		rdBAluno.setBounds(15, 80, 110, 25);
		getContentPane().add(rdBAluno);
		
		rdBProfessor = new JRadioButton("Professor");
		rdBGroup.add(rdBProfessor);
		rdBProfessor.setBackground(Color.WHITE);
		rdBProfessor.setHorizontalAlignment(SwingConstants.CENTER);
		rdBProfessor.setBounds(137, 80, 110, 25);
		getContentPane().add(rdBProfessor);
		
		rdBFuncionario = new JRadioButton("Funcion\u00E1rio");
		rdBGroup.add(rdBFuncionario);
		rdBFuncionario.setBackground(Color.WHITE);
		rdBFuncionario.setHorizontalAlignment(SwingConstants.CENTER);
		rdBFuncionario.setBounds(259, 80, 110, 25);
		getContentPane().add(rdBFuncionario);
		
		
		jTableUsuario = montarJTable();
		jTableUsuario.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		scrollPane = new JScrollPane(jTableUsuario);
		GroupLayout gl_jPanelTabela = new GroupLayout(jPanelTabela);
		gl_jPanelTabela.setHorizontalGroup(
			gl_jPanelTabela.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_jPanelTabela.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 953, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_jPanelTabela.setVerticalGroup(
			gl_jPanelTabela.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_jPanelTabela.createSequentialGroup()
					.addGap(5)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
					.addContainerGap())
		);
		jPanelTabela.setLayout(gl_jPanelTabela);
		
		JButton btnCancelar = new JButton("Fechar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnCancelar.setBounds(885, 443, 90, 20);
		getContentPane().add(btnCancelar);
		
		criarPopupMenu();		
		setVisible(true);		
	}
	

	

	
	private void criarPopupMenu() {
		jTableUsuario.addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseClicked(MouseEvent e) {				
				int linha = jTableUsuario.getSelectedRow();	
				if(linha == -1) {
					JOptionPane.showMessageDialog(null, "Selecione uma linha");
				}else{
					if(jTableUsuario.getValueAt(linha, 0) == null ) {						
						new TelaCadastro();
						
					}else {
						menuJPopUp = new JPopupMenu();
						JMenuItem editar 	= new  JMenuItem("Editar");
						JMenuItem excluir 	= new  JMenuItem("Excluir");

						editar.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {	
								String prontuario = (String) jTableUsuario.getValueAt(linha, 0);								
								String nome = (String) jTableUsuario.getValueAt(linha, 1);
								String email = (String) jTableUsuario.getValueAt(linha, 2);
								String tel = (String) jTableUsuario.getValueAt(linha, 3);
								String cpf = (String) jTableUsuario.getValueAt(linha, 4);
								String rg = (String) jTableUsuario.getValueAt(linha, 5);
								String data = (String) jTableUsuario.getValueAt(linha, 6);
								String tipoAssociado = (String) jTableUsuario.getValueAt(linha, 7);
								String logradouro = (String) jTableUsuario.getValueAt(linha, 8);
								String cidade = (String) jTableUsuario.getValueAt(linha, 9);
								String estado = (String) jTableUsuario.getValueAt(linha, 10);
								String endereco = (String) jTableUsuario.getValueAt(linha, 11);
								String cep = (String) jTableUsuario.getValueAt(linha, 12);
								String num = (String) jTableUsuario.getValueAt(linha, 13);								
								
								System.out.println(data);
								new EditarAssociado(nome, prontuario, email, tipoAssociado, cpf, rg, tel, data, endereco, logradouro, cidade, cep, estado, num, true);
					
								
							}

						});						
						excluir.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								int x = JOptionPane.showConfirmDialog(null, "Deseja Associado: " + jTableUsuario.getValueAt(linha, 0), "Excluir", JOptionPane.YES_NO_OPTION);
								if(x == JOptionPane.YES_OPTION) {
									String where = (String) jTableUsuario.getValueAt(linha, 0);
									System.out.println(where);
									Aluno a = new Aluno(where);
									if(a.excluiAssociado()) {
										System.out.println("Certo");
									}else {
										System.out.println("Erro");
									}
								}
								refresh();
							}
						});
						
						menuJPopUp.add(editar);
						menuJPopUp.add(excluir);
						
						
						jTableUsuario.addMouseListener(new MouseAdapter() {
							@Override
							public void mouseReleased(MouseEvent e) {
								if (jTableUsuario.getSelectedRow() != -1)  {
									showPopup(e);
								}
							}
						});
					}				
					
				}
			}
			public void showPopup(MouseEvent e) {
				if(e.isPopupTrigger()){
					menuJPopUp.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});		
		
	}
	
	
	public void refresh() {
		jPanelTabela.remove(scrollPane);
		if(txtBuscar.getText().isEmpty()) {
			jTableUsuario = montarJTable();
		}else {
			String where = txtBuscar.getText().replace("*", "%");
			jTableUsuario = montarJtable( "Prontuario ='"+where+"' OR nome like '"+where+ "'");						
		}
		scrollPane = new JScrollPane(jTableUsuario);
		GroupLayout gl_jPanelTabela = new GroupLayout(jPanelTabela);
		gl_jPanelTabela.setHorizontalGroup(
			gl_jPanelTabela.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_jPanelTabela.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 953, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_jPanelTabela.setVerticalGroup(
			gl_jPanelTabela.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_jPanelTabela.createSequentialGroup()
					.addGap(5)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
					.addContainerGap())
		);
		jPanelTabela.setLayout(gl_jPanelTabela);
		criarPopupMenu();
		validate();
		repaint();
		
	}
	
	private JTable montarJTable() {
		return(montarJtable(""));
	}
	
	private JTable montarJtable(String where) {
		JTable tmpTable = null;		
		ResultSet rs = new Aluno().carregarDados(where);		
		
		dbMySQL.ResultSetExtras rsExtras = new dbMySQL.ResultSetExtras(rs);
		tmpTable = rsExtras.getjTable();
		return (tmpTable);
	}
}
