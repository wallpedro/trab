package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;
import javax.swing.border.BevelBorder;

public class TelaCadastro extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel 					contentPane;
	private final ButtonGroup 		buttonGroup = new ButtonGroup();	
	private JTextField 				txtNome;
	private JTextField 				txtProntuario;
	private JTextField 				txtEmail;
	private JRadioButton 			rbServidor;
	private JRadioButton 			rbAluno;
	private JRadioButton 			rbProfessor;

	public TelaCadastro() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(TelaCadastro.class.getResource("/img/icon_usuario.png")));
		setResizable(false);
		setTitle("Cadastro de Associado - Biblioteca");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 400, 350);
		setLocationRelativeTo(null);	
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		

		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Informa\u00E7\u00F5es B\u00E1sicas ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 0, 395, 285);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(10, 20, 100, 20);
		panel.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setColumns(10);
		txtNome.setBounds(125, 20, 250, 20);
		panel.add(txtNome);
		
		JLabel lblProntuario = new JLabel("Prontu�rio:");
		lblProntuario.setBounds(10, 60, 100, 20);
		panel.add(lblProntuario);
		
		txtProntuario = new JTextField();
		txtProntuario.setColumns(10);
		txtProntuario.setBounds(125, 60, 180, 20);
		panel.add(txtProntuario);
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setBounds(10, 100, 100, 20);
		panel.add(lblEmail);

				
		JLabel lblTipo = new JLabel("Tipo Associado:");
		lblTipo.setBounds(10, 140, 100, 14);
		panel.add(lblTipo);
		
		rbProfessor = new JRadioButton("Professor");
		buttonGroup.add(rbProfessor);
		rbProfessor.setBackground(Color.WHITE);
		rbProfessor.setBounds(48, 161, 100, 25);
		panel.add(rbProfessor);
		
		rbAluno = new JRadioButton("Aluno");
		buttonGroup.add(rbAluno);
		rbAluno.setBackground(Color.WHITE);
		rbAluno.setBounds(48, 186, 100, 25);
		panel.add(rbAluno);
		
		rbServidor = new JRadioButton("Funcion�rio");
		buttonGroup.add(rbServidor);
		rbServidor.setBackground(Color.WHITE);
		rbServidor.setBounds(48, 211, 100, 25);
		panel.add(rbServidor);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(125, 100, 250, 20);
		panel.add(txtEmail);
		
		
		//Bot�o Cancelar
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(175, 290, 100, 25);
		btnCancelar.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();				
			}
		});
		contentPane.add(btnCancelar);
		
		//Bot�o Pr�ximo
		JButton btnProximo = new JButton("Pr�ximo");
		btnProximo.setBounds(285, 290, 100, 25);		
		btnProximo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Proximo();
			}
		});
		contentPane.add(btnProximo);
		setVisible(true);	
		
	}
	
	public void Voltar(String nome, String prontuario, String email, String associado ) {		
		if(rbAluno.getText() == associado) {
			rbAluno.setSelected(true);
		}else if(associado == rbProfessor.getText()) {
			rbProfessor.setSelected(true);
		}else if(associado == rbServidor.getText()) {
			rbServidor.setSelected(true);
		}
		this.txtNome.setText(nome);
		this.txtProntuario.setText(prontuario);
		this.txtEmail.setText(email);		
	}
	
	public void Proximo() {
		String associado = null;
		if(rbAluno.isSelected()) {
			associado = rbAluno.getText();
		}else if(rbProfessor.isSelected()) {
			associado = rbProfessor.getText();
		}else if(rbServidor.isSelected()){
			associado = rbServidor.getText();
		}		
		if(txtNome.getText().isEmpty() || txtProntuario.getText().isEmpty() || txtEmail.getText().isEmpty() || associado == null) {
			JOptionPane.showMessageDialog(null, "Informe todos os campos", "Error", JOptionPane.ERROR_MESSAGE);
		}else {			
			new TelaCadastro2(txtNome.getText(), txtProntuario.getText(), txtEmail.getText(), associado);
			//setVisible(false);
			dispose();
		}		
	}	
}
