package view;

import java.awt.Color;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import Entidades.Aluno;
import Entidades.Funcionario;
import Entidades.Professor;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaConfirm extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private JPanel 		contentPane;
	private JTextField 	txtNome;
	private JTextField 	txtProntuario;
	private JTextField 	txtEmail;
	private JTextField 	txtTipoAssociado;
	private JLabel 		lblCPF;
	private JTextField 	txtCPF;
	private JLabel 		lblRG;
	private JTextField 	txtRG;
	private JLabel 		lblTelefone;
	private JTextField 	txtTelefone;
	private JLabel 		lblNascimento;
	private JTextField 	txtDia;
	private JLabel 		lblDia;
	private JLabel 		lblMes;
	private JTextField 	txtMes;
	private JTextField 	txtAno;
	private JLabel 		lblAno;
	private JLabel 		lblEndereco;
	private JTextField 	txtEndereco;
	private JLabel 		lblLogradouro;
	private JTextField 	txtLogradouro;
	private JLabel 		lblCidade;
	private JTextField 	txtCidade;
	private JLabel 		lblCEP;
	private JTextField 	txtCEP;
	private JTextField 	txtNumero;
	private JLabel 		lblNumero;
	private JTextField 	txtEstado;
	private JLabel 		lblEstado;
	private JButton 	btnVoltar;
	private JButton 	btnConfirm;
	
	private JLabel 		lblCargo;
	private JTextField 	txtComplemento;
	private JLabel 		lblFormacao;
	private JLabel 		lblModulo;
	private Boolean 	exibir;

	//
	public TelaConfirm(String nome, String prontuario, String email, String associado, String cpf, String rg, String telefone, String dia, String mes, String ano, String endereco, String logradouro, String cidade, String cep, String estado, String numero, String complemento, Boolean exib) {
		this.exibir = exib;
		setTitle("Confirma��o de Associado - Biblioteca");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 500, 500);
		
		setResizable(false);
		setLocationRelativeTo(null);
		setIconImage(Toolkit.getDefaultToolkit().getImage(TelaConfirm.class.getResource("/img/icon_usuario.png")));
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);		
		getContentPane().setBackground(Color.WHITE);
		
		JPanel panelBasico = new JPanel();
		panelBasico.setLayout(null);
		panelBasico.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Confirma��o dos Dados ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelBasico.setBackground(Color.WHITE);
		panelBasico.setBounds(0, 0, 495, 305);
		contentPane.add(panelBasico);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(10, 25, 100, 20);
		panelBasico.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setEnabled(exibir);
		txtNome.setColumns(10);
		txtNome.setBounds(120, 25, 250, 20);
		panelBasico.add(txtNome);
		
		JLabel lblProntuario = new JLabel("Prontu�rio:");
		lblProntuario.setBounds(10, 50, 100, 20);
		panelBasico.add(lblProntuario);
		
		txtProntuario = new JTextField();
		txtProntuario.setEnabled(exibir);
		txtProntuario.setColumns(10);
		txtProntuario.setBounds(120, 50, 180, 20);
		panelBasico.add(txtProntuario);
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setBounds(10, 75, 100, 20);
		panelBasico.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setEnabled(exibir);
		txtEmail.setColumns(10);
		txtEmail.setBounds(120, 75, 250, 20);
		panelBasico.add(txtEmail);
		
		lblCPF = new JLabel("CPF:");
		lblCPF.setBounds(10, 100, 80, 20);
		panelBasico.add(lblCPF);
		
		txtCPF = new JTextField();
		txtCPF.setEnabled(exibir);
		txtCPF.setColumns(10);
		txtCPF.setBounds(120, 100, 265, 20);
		panelBasico.add(txtCPF);
		
		lblRG = new JLabel("RG:");
		lblRG.setBounds(10, 125, 80, 20);
		panelBasico.add(lblRG);
		
		txtRG = new JTextField();
		txtRG.setEnabled(exibir);
		txtRG.setColumns(10);
		txtRG.setBounds(120, 125, 265, 20);
		panelBasico.add(txtRG);
		
		lblTelefone = new JLabel("Telefone:");
		lblTelefone.setBounds(10, 150, 80, 20);
		panelBasico.add(lblTelefone);
		
		txtTelefone = new JTextField();
		txtTelefone.setEnabled(exibir);
		txtTelefone.setColumns(10);
		txtTelefone.setBounds(120, 150, 150, 20);
		panelBasico.add(txtTelefone);
		
		lblNascimento = new JLabel("Nascimento:");
		lblNascimento.setBounds(10, 175, 80, 20);
		panelBasico.add(lblNascimento);
		
		txtDia = new JTextField();
		txtDia.setEnabled(exibir);
		txtDia.setColumns(10);
		txtDia.setBounds(155, 175, 30, 20);
		panelBasico.add(txtDia);
		
		lblDia = new JLabel("Dia:");
		lblDia.setBounds(120, 175, 30, 20);
		panelBasico.add(lblDia);
		
		lblMes = new JLabel("M�s:");
		lblMes.setBounds(195, 175, 30, 20);
		panelBasico.add(lblMes);
		
		txtMes = new JTextField();
		txtMes.setEnabled(exibir);
		txtMes.setColumns(10);
		txtMes.setBounds(230, 175, 30, 20);
		panelBasico.add(txtMes);
		
		txtAno = new JTextField();
		txtAno.setEnabled(exib);
		txtAno.setColumns(10);
		txtAno.setBounds(305, 175, 40, 20);
		panelBasico.add(txtAno);
		
		lblAno = new JLabel("Ano:");
		lblAno.setBounds(270, 175, 30, 20);
		panelBasico.add(lblAno);
		
		lblEndereco = new JLabel("Endere�o:");
		lblEndereco.setBounds(10, 200, 100, 20);
		panelBasico.add(lblEndereco);
		
		txtEndereco = new JTextField();
		txtEndereco.setEnabled(exibir);
		txtEndereco.setColumns(10);
		txtEndereco.setBounds(120, 200, 265, 20);
		panelBasico.add(txtEndereco);
		
		lblLogradouro = new JLabel("Logradouro:");
		lblLogradouro.setBounds(10, 225, 100, 20);
		panelBasico.add(lblLogradouro);
		
		txtLogradouro = new JTextField();
		txtLogradouro.setEnabled(exibir);
		txtLogradouro.setColumns(10);
		txtLogradouro.setBounds(120, 225, 60, 20);
		panelBasico.add(txtLogradouro);
		
		lblCidade = new JLabel("Cidade:");
		lblCidade.setBounds(10, 250, 100, 20);
		panelBasico.add(lblCidade);
		
		txtCidade = new JTextField();
		txtCidade.setEnabled(exibir);
		txtCidade.setColumns(10);
		txtCidade.setBounds(120, 250, 100, 20);
		panelBasico.add(txtCidade);
		
		lblCEP = new JLabel("CEP:");
		lblCEP.setBounds(230, 250, 60, 20);
		panelBasico.add(lblCEP);
		
		txtCEP = new JTextField();
		txtCEP.setEnabled(exibir);
		txtCEP.setColumns(10);
		txtCEP.setBounds(300, 250, 85, 20);
		panelBasico.add(txtCEP);
		
		txtNumero = new JTextField();
		txtNumero.setEnabled(exibir);
		txtNumero.setColumns(10);
		txtNumero.setBounds(300, 275, 50, 20);
		panelBasico.add(txtNumero);
		
		lblNumero = new JLabel("N�mero:");
		lblNumero.setBounds(230, 275, 60, 20);
		panelBasico.add(lblNumero);
		
		txtEstado = new JTextField();
		txtEstado.setEnabled(exibir);
		txtEstado.setColumns(10);
		txtEstado.setBounds(120, 275, 100, 20);
		panelBasico.add(txtEstado);
		
		lblEstado = new JLabel("Estado:");
		lblEstado.setBounds(10, 275, 100, 20);
		panelBasico.add(lblEstado);
		
		btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Volta(nome, prontuario, email, associado, cpf, rg, telefone, dia, mes, ano, endereco, logradouro, cidade, cep, estado, numero, complemento);				
			}
		});
		btnVoltar.setBounds(275, 440, 100, 25);
		contentPane.add(btnVoltar);
		
		btnConfirm = new JButton("Confirmar");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				if(CriarClass(nome, prontuario, email, associado, cpf, rg, telefone, dia, mes, ano, endereco, logradouro, cidade, cep, estado, numero, complemento)) {
					dispose();
				}else {
					JOptionPane.showInternalMessageDialog(null,"Erro");
				}
				
			}
		});
		btnConfirm.setBounds(385, 440, 100, 25);
		contentPane.add(btnConfirm);
		contentPane.add(Associado(associado));
		Preencher(nome, prontuario, email, associado, cpf, rg, telefone, dia, mes, ano, endereco, logradouro, cidade, cep, estado, numero, complemento);
		setVisible(true);
	}
	
	protected boolean CriarClass(String nome, String prontuario, String email, String associado, String cpf, String rg, String telefone, String dia, String mes, String ano, String endereco, String logradouro, String cidade, String cep, String estado, String numero, String complemento) {
		if(associado == "Aluno") {			
			Aluno aluno = new Aluno(prontuario, nome, email, telefone, cpf, rg, dia, mes, ano, logradouro, endereco, numero, cidade, estado, cep, Integer.parseInt(complemento));
			if(aluno.cadastraAssociado(aluno.toArray())) {
				return true;
			}else {
				return false;
			}
		}else if(associado.equals("Professor")) {
			Professor professor = new Professor(prontuario, nome, email, telefone, cpf, rg, dia, mes, ano, logradouro, endereco, numero, cidade, estado, cep, complemento);
			if(professor.cadastraAssociado(professor.toArray())){
				return true;
			}else {
				return false;
			}
		}else if(associado.equals("Funcion�rio")) {
			Funcionario funcionario = new Funcionario(prontuario, nome, email, telefone, cpf, rg, dia, mes, ano, logradouro, endereco, numero, cidade, estado, cep, complemento);
			if(funcionario.cadastraAssociado(funcionario.toArray())) {
				return true;
			}else {
				return false;
			}
				
		}
		return false;
	}

	public JPanel Associado(String associado) {		
		JPanel JPanelAssociado = new JPanel();
		
		if(associado.equals("Aluno")) {			
			
			JPanelAssociado.setLayout(null);
			JPanelAssociado.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Tipo de Associado ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			JPanelAssociado.setBackground(Color.WHITE);
			JPanelAssociado.setBounds(0, 310, 495, 120);
			contentPane.add(JPanelAssociado);
			
			JLabel lblTipoAssociado = new JLabel("Tipo Associado:");
			lblTipoAssociado.setBounds(10, 25, 100, 20);
			JPanelAssociado.add(lblTipoAssociado);
			
			txtTipoAssociado = new JTextField();
			txtTipoAssociado.setEnabled(exibir);
			txtTipoAssociado.setColumns(10);
			txtTipoAssociado.setBounds(120, 25, 250, 20);
			JPanelAssociado.add(txtTipoAssociado);
			
			lblModulo = new JLabel("Formac�o:");
			lblModulo.setBounds(10, 50, 100, 20);
			JPanelAssociado.add(lblModulo);
			
			txtComplemento = new JTextField();
			txtComplemento.setEnabled(exibir);
			txtComplemento.setColumns(10);
			txtComplemento.setBounds(120, 50, 250, 20);
			JPanelAssociado.add(txtComplemento);
			
		}else if(associado.equals("Professor")) {
			
			JPanelAssociado.setLayout(null);
			JPanelAssociado.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Tipo de Associado ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			JPanelAssociado.setBackground(Color.WHITE);
			JPanelAssociado.setBounds(0, 310, 495, 120);
			contentPane.add(JPanelAssociado);
			
			JLabel lblTipoAssociado = new JLabel("Tipo Associado");
			lblTipoAssociado.setBounds(10, 25, 100, 20);
			JPanelAssociado.add(lblTipoAssociado);
			
			txtTipoAssociado = new JTextField();
			txtTipoAssociado.setEnabled(exibir);
			txtTipoAssociado.setColumns(10);
			txtTipoAssociado.setBounds(120, 25, 250, 20);
			JPanelAssociado.add(txtTipoAssociado);
			
			lblFormacao = new JLabel("Formac�o:");
			lblFormacao.setBounds(10, 50, 100, 20);
			JPanelAssociado.add(lblFormacao);
			
			txtComplemento = new JTextField();
			txtComplemento.setEnabled(exibir);
			txtComplemento.setColumns(10);
			txtComplemento.setBounds(120, 50, 250, 20);
			JPanelAssociado.add(txtComplemento);		

			
		}else if(associado.equals("Funcion�rio")) {			
			
			JPanelAssociado.setLayout(null);
			JPanelAssociado.setBorder(new TitledBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)), " Tipo de Associado ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			JPanelAssociado.setBackground(Color.WHITE);
			JPanelAssociado.setBounds(0, 310, 495, 120);
			contentPane.add(JPanelAssociado);
			
			JLabel lblTipoAssociado = new JLabel("Tipo Associado:");
			lblTipoAssociado.setBounds(10, 25, 100, 20);
			JPanelAssociado.add(lblTipoAssociado);
			
			txtTipoAssociado = new JTextField();
			txtTipoAssociado.setEnabled(exibir);
			txtTipoAssociado.setColumns(10);
			txtTipoAssociado.setBounds(120, 25, 250, 20);
			JPanelAssociado.add(txtTipoAssociado);
			
			lblCargo = new JLabel("Formac�o:");
			lblCargo.setBounds(10, 50, 100, 20);
			JPanelAssociado.add(lblCargo);
			
			txtComplemento = new JTextField();
			txtComplemento.setEnabled(exibir);
			txtComplemento.setColumns(10);
			txtComplemento.setBounds(120, 50, 250, 20);
			JPanelAssociado.add(txtComplemento);
			
		}
		return JPanelAssociado;
	}
	
	public void Preencher(String nome, String prontuario, String email, String associado, String cpf, String rg, String telefone, String dia, String mes, String ano, String endereco, String logradouro, String cidade, String cep, String estado, String numero, String complemneto) {
		this.txtNome.setText(nome);
		this.txtProntuario.setText(prontuario);
		this.txtEmail.setText(email);
		this.txtTipoAssociado.setText(associado);
		this.txtCPF.setText(cpf);
		this.txtRG.setText(rg);
		this.txtTelefone.setText(telefone);
		this.txtDia.setText(dia);
		this.txtMes.setText(mes);
		this.txtAno.setText(ano);
		this.txtEndereco.setText(endereco);
		this.txtLogradouro.setText(logradouro);
		this.txtCidade.setText(cidade);
		this.txtCEP.setText(cep);
		this.txtEstado.setText(estado);
		this.txtNumero.setText(numero);
		this.txtComplemento.setText(complemneto);
	}
	public void Volta(String nome, String prontuario, String email, String associado, String cpf, String rg, String telefone, String dia, String mes, String ano, String endereco, String logradouro, String cidade, String cep, String estado, String numero, String complemento) {
		new TelaCadastro2(nome, prontuario, email, associado).Voltar(cpf, rg, telefone, dia, mes, ano, endereco, logradouro, cidade, cep, estado, numero, complemento);
		setVisible(false);
		
	}

}
