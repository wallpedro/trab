package Entidades;

import dbMySQL.DbQuery_MySQL;

public class Revista extends Acervo {
	
	private int idRevista;
	private int ca;
	private int idEditora;
	
	private String table = "revista";
	private String campos = "IdRevista,CA,IdEditora";
	private String keyField = "CA";
	
	public Revista(int ca) {
		super(ca);
		this.foreignDataObject = new DbQuery_MySQL(this.table, this.campos, this.keyField);
	}
	
	public Revista(int ca, String titulo, int ano, int mes, int quantidadeExemplares, int idEditora) {
		super(ca, titulo, ano, mes, quantidadeExemplares);
		this.foreignDataObject = new DbQuery_MySQL(this.table, this.campos, this.keyField);
		this.setIdEditora(idEditora);
	}
	
	public String[] toArray() {
		return new String[] {
				Integer.toString(this.getIdRevista()),
				Integer.toString(this.getCa()),
				Integer.toString(this.getIdEditora())
		};
	}

	public int getIdRevista() {
		return idRevista;
	}

	public void setIdRevista(int idRevista) {
		this.idRevista = idRevista;
	}

	public int getCa() {
		return ca;
	}

	public void setCa(int ca) {
		this.ca = ca;
	}

	public int getIdEditora() {
		return idEditora;
	}

	public void setIdEditora(int idEditora) {
		this.idEditora = idEditora;
	}
	
	

}
