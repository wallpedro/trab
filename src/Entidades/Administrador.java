package Entidades;

import java.sql.ResultSet;
import java.sql.SQLException;

import dbMySQL.DbQuery_MySQL;

public class Administrador {
	
	private String login;
	private String pass;
	
	private String usuarioTable 	= "administradores";
	private String usuarioCampos 	= "Login, Senha";
	private String usuarioKeyField	= "IdAdmn";
	
	protected dbMySQL.DbQuery_MySQL adminDataObj = null;
	public Administrador(String login, String senha) {		
		this.login 	= 	login;
		this.pass 	= 	senha;	
		this.adminDataObj = new DbQuery_MySQL(this.usuarioTable, this.usuarioCampos, this.usuarioKeyField);
	}		
	
	public boolean ConfirmLogin() {
		ResultSet rs = this.adminDataObj.select("Login='"+this.getLogin()+"' and Senha='"+this.getPass()+"'");		
		try {
			return(rs.next());
		} catch (SQLException e) {
			return (false);
		}	
	}

	public String getLogin() {
		return login;
	}

	public String getPass() {
		return pass;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
		
		

}
