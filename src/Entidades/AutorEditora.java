package Entidades;

import dbMySQL.DbQuery_MySQL;

public class AutorEditora {
	
	private String nome;
	private int id = 0;
	private DbQuery_MySQL qry = null;
	
	public AutorEditora(int id) {
		this.qry = new DbQuery_MySQL();
		this.setId(id);
	}
	
	public AutorEditora(String nome) {
		this.setNome(nome);
	}
	
	public boolean cadastraAutor() {
		if(this.getNome() != null) {
			this.qry.executeUpdate("INSERT INTO autor(Nome) VALUE('" + this.getNome() + "'");
			return (true);
		}
		return(false);
	}
	
	public boolean cadastraEditora() {
		if(this.getNome() != null) {
			this.qry.executeUpdate("INSERT INTO editora(Nome) VALUE('" + this.getNome() + "'");
			return (true);
		}
		return(false);
	}
	
	public boolean removeEditora() {
		if(this.getId() != 0) {
			this.qry.executeUpdate("DELETE FROM editora WHERE IdEditora = " + this.getId());
			return (true);
		}
		return(false);
	}
	
	public boolean removeAutor() {
		if(this.getId() != 0) {
			this.qry.executeUpdate("DELETE FROM autor WHERE IdEditora = " + this.getId());
			return (true);
		}
		return(false);
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
