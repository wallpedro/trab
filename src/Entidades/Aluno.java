package Entidades;


import dbMySQL.DbQuery_MySQL;
@SuppressWarnings("unused")
public class Aluno extends Associado  {
	
	private final int TIPO_ASSOCIADO = 1;
	private final int PRAZO_ENTREGA = 7;
	private final int QTD_LIVROS_MAX = 2;

	private int Modulo;
	
	private String alunoTable 		= "aluno";
	private String alunoKeyField 	= "Prontuario";
	private String alunoCampos 		= "Prontuario,Modulo,PrazoEntrega";
	
	public Aluno() {
		super();
	}
	
	public Aluno(String prontuario) {
		super(prontuario);
	}

	public Aluno(String prontuario, String nome, String email, String telefone, String cpf, String rg,
			String dia, String mes, String ano, String logradouro, String endereco, String numero, String cidade, String estado,
			String CEP, int modulo) {
		super(prontuario, nome, email, telefone, cpf, rg, dia, mes, ano, logradouro, endereco, numero, cidade, estado, CEP);
		this.setModulo(modulo);
		this.setPrazoEntrega(PRAZO_ENTREGA);
		this.subclassDataObject = new DbQuery_MySQL(this.alunoTable, this.alunoCampos, this.alunoKeyField);
		this.setTipoAssociado(TIPO_ASSOCIADO);
	}
	
	public String[] toArray() {
		return new String[] {
				this.getProntuario(),
				Integer.toString(this.getModulo()),
				Integer.toString(this.getPrazoEntrega())
		};
	}
	
	public int getTIPO_ASSOCIADO() {
		return TIPO_ASSOCIADO;
	}

	public int getPRAZO_ENTREGA() {
		return PRAZO_ENTREGA;
	}

	public int getQTD_LIVROS_MAX() {
		return QTD_LIVROS_MAX;
	}
	
	public int getModulo() {
		return Modulo;
	}

	public void setModulo(int modulo) {
		Modulo = modulo;
	}
}
