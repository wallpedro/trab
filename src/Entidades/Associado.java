package Entidades;

import java.sql.ResultSet;
import java.sql.SQLException;

import dbMySQL.DbQuery_MySQL;

public abstract class Associado{

	private String 			prontuario = "";
	private String 			nome;
	private String 			email;
	private String 			telefone;
	private String 			cpf;
	private String 			rg;
	private String 			diaNascimento;
	private String 			mesNascimento;
	private String 			anoNascimento;
	private String 			logradouro;
	private String 			endereco;
	private String 			numero;
	private String 			cidade;
	private String 			estado;
	private String 			CEP;
	private int 			prazoEntrega;
	private int 			ativo = 1;
	private int 			tipoAssociado;
	protected DbQuery_MySQL associadoDataObject = null;
	protected DbQuery_MySQL subclassDataObject = null;
	protected String 		associadoTable = "associados";
	protected String 		associadoCampos = "Prontuario,Nome,Email,Telefone,CPF,RG,DataNascimento,TipoAssociado,Logradouro,Cidade,Estado,Endereco,CEP,Numero,Ativo";
	protected String 		associadoKeyField = "Prontuario";
	
	public Associado() {
		this.associadoDataObject = new DbQuery_MySQL(this.associadoTable, this.associadoCampos, this.associadoKeyField);
	}
	
	public Associado(String prontuario) {
		this.setProntuario(prontuario);
		this.associadoDataObject = new DbQuery_MySQL(this.associadoTable, this.associadoCampos, this.associadoKeyField);
	}
	
	public Associado(String prontuario, String nome, String email, String telefone, String cpf, 
			String rg, String dia, String mes, String ano, String logradouro, String endereco, String numero, String cidade, String estado, String CEP)
	{
		this.setProntuario(prontuario);
		this.setNome(nome);
		this.setEmail(email);
		this.setTelefone(telefone);
		this.setCpf(cpf);
		this.setRg(rg);
		this.setDiaNascimento(dia);
		this.setMesNascimento(mes);
		this.setAnoNascimento(ano);
		this.setLogradouro(logradouro);
		this.setEndereco(endereco);
		this.setNumero(numero);
		this.setCidade(cidade);
		this.setEstado(estado);
		this.setCEP(CEP);
		this.associadoDataObject = new DbQuery_MySQL(this.associadoTable, this.associadoCampos, this.associadoKeyField);
	}
	
	
	//Apenas para exibi��o do Dados em Tela.
	public ResultSet carregarDados(String where){
		if(where == "") {
			ResultSet rs = this.associadoDataObject.select("");		
			return rs;
		}else {
			return(this.associadoDataObject.select(where));
		}
			
	}
	
	
	public abstract int getPRAZO_ENTREGA();
	
	public boolean cadastraAssociado(String[] arrayDados){
		if(this.associadoExiste() && this.associadoExisteAtivo()) {
			return (false);
		}else if(this.associadoExiste() && !this.associadoExisteAtivo()) {
			this.associadoDataObject.update(this.toArrayAssociado());			
			this.subclassDataObject.update(arrayDados);
			return (true);
		}
		this.associadoDataObject.insert(this.toArrayAssociado());
		this.subclassDataObject.insert(arrayDados);
		return (true);
	}
	
	public boolean editaAssociado(String[] arrayDados) {
		if(this.associadoExiste()) {
			this.associadoDataObject.update(this.toArrayAssociado());
			this.subclassDataObject.update(arrayDados);
			return (true);
		}
		return(false);
	}
	
	protected String[] toArrayAssociado() {
		return new String[] {
					this.getProntuario(),
					this.getNome(),
					this.getEmail(),
					this.getTelefone(),
					this.getCpf(),
					this.getRg(),
					this.getDataNascimentoParaBD(),
					Integer.toString(this.getTipoAssociado()),
					this.getLogradouro(),
					this.getCidade(),
					this.getEstado(),
					this.getEndereco(),
					this.getCEP(),
					this.getNumero(),
					Integer.toString(getAtivo())
		};
	}
	
	protected boolean associadoExiste() {
		ResultSet rs = this.associadoDataObject.select("Prontuario = '" + this.getProntuario() + "'");
		try {
			return rs.next();
		}catch(SQLException e){
			return(false);
		}
	}
	
	protected boolean associadoExisteAtivo() {
		ResultSet rs = this.associadoDataObject.select("Prontuario = '" + this.getProntuario() + "' AND Ativo = 1");
		try {
			return rs.next();
		}catch(SQLException e){
			return(false);
		}
	}
	
	public boolean excluiAssociado() {
		if(this.associadoExiste()) {
			this.setAtivo(0);
			//this.associadoDataObject.update(this.toArrayAssociado());
			this.associadoDataObject.desativa(this.getProntuario(), this.getAtivo());
			return (true);
		}
		return (false);
	}
	
	public abstract String[] toArray();
	
	public int getTipoAssociado() {
		return tipoAssociado;
	}

	public void setTipoAssociado(int tipoAssociado) {
		this.tipoAssociado = tipoAssociado;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}
	
	public String getProntuario() {
		return prontuario;
	}

	public void setProntuario(String prontuario) {
		this.prontuario = prontuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}
	
	public String getDataNascimento() {
		return this.getDiaNascimento() + "/" + this.getMesNascimento() + "/" + this.getAnoNascimento();
	}
	
	public String getDataNascimentoParaBD() {
		return this.getAnoNascimento() + "-" + this.getMesNascimento() + "-" + this.getDiaNascimento();
	}

	public String getDiaNascimento() {
		return diaNascimento;
	}

	public void setDiaNascimento(String diaNascimento) {
		this.diaNascimento = diaNascimento;
	}

	public String getMesNascimento() {
		return mesNascimento;
	}

	public void setMesNascimento(String mesNascimento) {
		this.mesNascimento = mesNascimento;
	}

	public String getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(String anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCEP() {
		return CEP;
	}

	public void setCEP(String cEP) {
		CEP = cEP;
	}

	public int getPrazoEntrega() {
		return prazoEntrega;
	}

	public void setPrazoEntrega(int prazoEntrega) {
		this.prazoEntrega = prazoEntrega;
	}
}
