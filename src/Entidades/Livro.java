package Entidades;

import dbMySQL.DbQuery_MySQL;

public class Livro extends Acervo {
	
	private int idLivro;
	private int isbn;
	private int ca;
	private int edicao;
	private int idEditora;
	
	private String table = "livro";
	private String campos = "IdLivro,ISBN,CA,Edicao,IdEditora";
	private String keyField = "CA";
	
	public Livro(int ca) {
		super(ca);
		this.foreignDataObject = new DbQuery_MySQL(this.table, this.campos, this.keyField);
	}
	
	public Livro(int ca, String titulo, int ano, int mes, int quantidadeExemplares, int isbn, int edicao, int idEditora) {
		super(ca, titulo, ano, mes, quantidadeExemplares);
		this.foreignDataObject = new DbQuery_MySQL(this.table, this.campos, this.keyField);
		this.setIsbn(isbn);
		this.setEdicao(edicao);
		this.setIdEditora(idEditora);
	}
	
	public String[] toArray() {
		return new String[] {
				Integer.toString(this.getIdLivro()),
				Integer.toString(this.getIsbn()),
				Integer.toString(this.getCa()),
				Integer.toString(this.getEdicao()),
				Integer.toString(this.getIdEditora())
		};
	}

	public int getIdLivro() {
		return idLivro;
	}

	public void setIdLivro(int idLivro) {
		this.idLivro = idLivro;
	}

	public int getIsbn() {
		return isbn;
	}

	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}

	public int getCa() {
		return ca;
	}

	public void setCa(int ca) {
		this.ca = ca;
	}

	public int getEdicao() {
		return edicao;
	}

	public void setEdicao(int edicao) {
		this.edicao = edicao;
	}

	public int getIdEditora() {
		return idEditora;
	}

	public void setIdEditora(int idEditora) {
		this.idEditora = idEditora;
	}

	
	
	

}
