package Entidades;

import dbMySQL.DbQuery_MySQL;

@SuppressWarnings("unused")
public class Professor extends Associado{
	
	private final int TIPO_ASSOCIADO = 2;
	private final int PRAZO_ENTREGA = 28;
	private final int QTD_LIVROS_MAX = 4;
	
	private String table 	= "professores";
	private String keyfield = "Prontuario";
	private String campos 	= "Prontuario,Formacao,PrazoEntrega";
	
	private String formacao;
	
	public Professor() {
		super();
	}
	
	public Professor(String prontuario) {
		super(prontuario);
	}
	
	public Professor(String prontuario, String nome, String email, String telefone, String cpf, String rg, String dia,
			String mes, String ano, String logradouro, String endereco, String numero, String cidade, String estado,
			String CEP, String formacao) {
		super(prontuario, nome, email, telefone, cpf, rg, dia, mes, ano, logradouro, endereco, numero, cidade, estado, CEP);
		this.setFormacao(formacao);
		this.setTipoAssociado(TIPO_ASSOCIADO);
		this.setPrazoEntrega(PRAZO_ENTREGA);
		this.subclassDataObject = new DbQuery_MySQL(this.table, this.campos, this.keyfield);
	}

	public String[] toArray() {
		return new String[] {
				this.getProntuario(),
				this.getFormacao(),
				Integer.toString(this.getPrazoEntrega())
		};
	} 
	
	
	
	public int getTIPO_ASSOCIADO() {
		return TIPO_ASSOCIADO;
	}

	public int getPRAZO_ENTREGA() {
		return PRAZO_ENTREGA;
	}

	public int getQTD_LIVROS_MAX() {
		return QTD_LIVROS_MAX;
	}

	public String getFormacao() {
		return formacao;
	}

	public void setFormacao(String formacao) {
		this.formacao = formacao;
	}

}
