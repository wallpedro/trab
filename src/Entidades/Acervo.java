package Entidades;

import java.sql.ResultSet;
import java.sql.SQLException;

import dbMySQL.DbQuery_MySQL;

public abstract class Acervo {
	
	private int ca;
	private String titulo;
	private int ano;
	private int mes;
	private int qtdExemplares;
	
	private String tableAcervo = "acervo";
	private String camposAcervo = "CA,Titulo,Ano,Mes";
	private String keyFieldAcervo = "CA";
	
	private String tableExemplar = "exemplar";
	private String camposExemplar = "CE,CA,emprestado";
	private String keyFieldExemplar = "CE";
	
	private DbQuery_MySQL acervoDataObject = null;
	private DbQuery_MySQL exemplarDataObject = null;
	protected DbQuery_MySQL foreignDataObject = null;
	
	
	public Acervo(int ca) {
		this.acervoDataObject = new DbQuery_MySQL(this.tableAcervo, this.camposAcervo, this.keyFieldAcervo);
		this.exemplarDataObject = new DbQuery_MySQL(this.tableExemplar, this.camposExemplar, this.keyFieldExemplar);
		this.setCa(ca);
		if(this.acervoExiste()) {
			this.setAtributos();
		}
	}
	
	public Acervo(int CA, String titulo, int ano, int mes, int quantidadeExemplares) {
		this.acervoDataObject = new DbQuery_MySQL(this.tableAcervo, this.camposAcervo, this.keyFieldAcervo);
		this.exemplarDataObject = new DbQuery_MySQL(this.tableExemplar, this.camposExemplar, this.keyFieldExemplar);
		this.setCa(CA);
		this.setTitulo(titulo);
		this.setAno(ano);
		this.setMes(mes);
		this.setQtdExemplares(quantidadeExemplares);
	}
	
	public boolean cadastraAcervo(String[] array) {
		//se existe, edita
		if(this.acervoExiste()) {
			int qtdInformada = this.getQtdExemplares();
			int qtdCadastrada = this.getQuantidadeExemplaresExistentes();
			if(qtdInformada >= qtdCadastrada) {
				this.acervoDataObject.update(this.toArrayAcervo());
				this.foreignDataObject.update(array);
				for(int i = 0; i < qtdInformada - qtdCadastrada; i++) {
					this.exemplarDataObject.insertFields("CA",this.toArrayExemplar());
				}
			}else{
				return (false);
			}
			return (true);
		}else {
			this.acervoDataObject.insert(this.toArrayAcervo());
			this.foreignDataObject.insert(array);
			for(int i = 0; i < this.getQtdExemplares(); i++) {
				this.exemplarDataObject.insertFields("CA",this.toArrayExemplar());
			}
			return(true);
		}
	}
	
	public boolean removeAcervo() {
		if(!this.existeExemplarEmprestado()) {
			if(this.deleteAllExemplar() && this.foreignDataObject.delete(Integer.toString(this.getCa())) == 0 && this.acervoDataObject.delete(Integer.toString(this.getCa())) == 0) {
				return (true);
			}
		}
		return (false);
	}
	
	private boolean deleteAllExemplar() {
		if(this.exemplarDataObject.deleteKey("CA", Integer.toString(this.getCa())) == 0) {
			return (true);
		}
		return(false);
	}
	
	private String[] toArrayAcervo() {
		return new String[] {
			Integer.toString(this.getCa()),
			this.getTitulo(),
			Integer.toString(this.getAno()),
			Integer.toString(this.getMes())
		};
	}
	
	private String[] toArrayExemplar() {
		return new String[] {
			Integer.toString(this.getCa()),
		};
	}
	
	private boolean existeExemplarEmprestado() {
		ResultSet rs = this.exemplarDataObject.select("emprestado = 1");
		try {
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return (false);
	}
	
	public abstract String[] toArray();
	
	protected boolean acervoExiste() {
		ResultSet rs = this.acervoDataObject.select("CA = '" + this.getCa() + "'");
		try {
			return rs.next();
		}catch(SQLException e){
			e.printStackTrace();
		}
		return (false);
	}
	
	private boolean setAtributos() {
		if(this.acervoExiste()) {
			ResultSet rs = this.acervoDataObject.select(this.keyFieldAcervo + " = '" + this.getCa() + "'");
			try {
				if(rs.next()) {
					this.setTitulo(rs.getString("Titulo"));
					this.setAno(rs.getInt("Ano"));
					this.setMes(rs.getInt("Mes"));
					this.setQtdExemplares(this.getQuantidadeExemplaresExistentes());
					return(true);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return (false);
	}
	
	public int getQuantidadeExemplaresExistentes() {
		if(this.acervoExiste()) {
			ResultSet rs = this.acervoDataObject.executeQuery("SELECT COUNT(*) AS qtd FROM exemplar WHERE CA = " + this.getCa());
			try {
				if(rs.next()) {
					return rs.getInt("qtd");
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	public int getCa() {
		return ca;
	}

	public void setCa(int ca) {
		this.ca = ca;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getQtdExemplares() {
		return qtdExemplares;
	}

	public void setQtdExemplares(int qtdExemplares) {
		this.qtdExemplares = qtdExemplares;
	}
}
