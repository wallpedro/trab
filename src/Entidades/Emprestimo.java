package Entidades;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;

import dbMySQL.DbQuery_MySQL;

public class Emprestimo {
	
	private final String DEFAULT_DATA_EMPRESTIMO = "CURDATE()";
	private String dataEmprestimo;
	private int prazo;
	private String dataDevolucao;
	private String prontuario;
	private int codExemplar;
	private DbQuery_MySQL emprestimoDataObject;
	
	private String table = "emprestimo";
	private String campos = "DataEmprestimo,Prazo,Prontuario,CE";
	private String keyfield = "IdEmprestimo";
	
	public Emprestimo(String prontuario, int codExemplar) {
		this.emprestimoDataObject = new DbQuery_MySQL(this.table, this.campos, this.keyfield);
		this.setProntuario(prontuario);
		this.setCodExemplar(codExemplar);
		this.setDados();
	}
	
	public void addEmprestimo() {
		if(this.getQtdEmprestado() != -1 && this.getMaxEmprestimosPorTipoUsuario() != -1 && this.getQtdEmprestado() <= this.getMaxEmprestimosPorTipoUsuario() && !this.exemplarIsEmprestado()) {
			this.emprestimoDataObject.insertCurDate(this.toArray());
			this.emprestimoDataObject.executeUpdate("UPDATE exemplar SET emprestado = 1 WHERE CE = " + this.getCodExemplar());
		}
	}
	
	public String[] toArray() {
		return new String[] {
				"!" + DEFAULT_DATA_EMPRESTIMO,
				"!" + DEFAULT_DATA_EMPRESTIMO + " + " + Integer.toString(this.getPrazo()),
				this.getProntuario(),
				Integer.toString(this.getCodExemplar())
		};
	}
	
	public void finalizaEmprestimo() {
		this.emprestimoDataObject.executeUpdate("UPDATE emprestimo SET DataDevolucao = CURDATE() WHERE Prontuario = '" + this.getProntuario() + "' AND CE = " + this.getCodExemplar());
		this.emprestimoDataObject.executeUpdate("UPDATE exemplar SET emprestado = 0 WHERE CE = " + this.getCodExemplar());
	}
	
	public void setDados() {
		this.setDataEmprestimo(DEFAULT_DATA_EMPRESTIMO);
		this.setPrazo(this.getPrazoPorUser());
		this.setMaxEmprestimos(this.getMaxEmprestimosPorTipoUsuario());
	}
	
	public void setMaxEmprestimos(int maxEmprestimos) {
	}

	public int getPrazoPorUser() {
		return this.getConstantAssociado("getPRAZO_ENTREGA");
	}
	
	private int getMaxEmprestimosPorTipoUsuario() {
		return this.getConstantAssociado("getQTD_LIVROS_MAX");
	}
	
	private int getConstantAssociado(String method) {
		String className = "Entidades." + this.getTipoAssociado();
		try {
			Object associado = className == "" ? null : Class.forName(className).newInstance();
			Method getPrazo = associado.getClass().getMethod(method);
			Object valor = getPrazo.invoke(associado, new Object [0]);
			return (int) valor;
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	private boolean exemplarIsEmprestado() {
		ResultSet rs = this.emprestimoDataObject.executeQuery("SELECT emprestado FROM exemplar WHERE CE = " + this.getCodExemplar());
		try {
			if(rs.next()) {
				return rs.getBoolean("emprestado");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return (true);
	}
	
	private String getTipoAssociado() {
		ResultSet rs = this.emprestimoDataObject.executeQuery("SELECT t.Nome FROM associados a INNER JOIN tipoassociado t on a.TipoAssociado = t.TipoAssociado AND a.Prontuario = '" + this.getProntuario() + "'");
		try {
			if(rs.next()) {
				return rs.getString("Nome");
			}
		} catch (SQLException e) {
			return "";
		}
		return "";
	}
	
	/**
	 * @return int: numero de livros emprestados ao usuario portador do corrente prontuario
	 */
	public int getQtdEmprestado() {
		ResultSet rs = this.emprestimoDataObject.executeQuery("SELECT COUNT(*) as qtdEmprestado FROM emprestimo WHERE Prontuario like '" + this.getProntuario() + "' AND DataDevolucao is null");
		try {
			if(rs.next()) {
				return rs.getInt("qtdEmprestado");
			}
		} catch (SQLException e) {
			return -1;		
		}
		return -1;
	}

	public String getDataEmprestimo() {
		return dataEmprestimo;
	}

	public void setDataEmprestimo(String dataEmprestimo) {
		this.dataEmprestimo = dataEmprestimo;
	}

	public int getPrazo() {
		return prazo;
	}

	public void setPrazo(int prazo) {
		this.prazo = prazo;
	}

	public String getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(String dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public String getProntuario() {
		return prontuario;
	}

	public void setProntuario(String prontuario) {
		this.prontuario = prontuario;
	}

	public int getCodExemplar() {
		return codExemplar;
	}

	public void setCodExemplar(int codExemplar) {
		this.codExemplar = codExemplar;
	}
	
}
