package Entidades;

import dbMySQL.DbQuery_MySQL;

@SuppressWarnings("unused")
public class Funcionario extends Associado {
	
	
	private final int TIPO_ASSOCIADO = 3;
	private final int PRAZO_ENTREGA = 14;
	private final int QTD_LIVROS_MAX = 2;
	
	private String table 	= "funcionarios";
	private String keyfield = "Prontuario";
	private String campos 	= "Prontuario,Cargo,PrazoEntrega";
	
	private String cargo;
	
	public Funcionario () {
		super();
	}

	public Funcionario(String prontuario, String nome, String email, String telefone, String cpf, String rg, String dia,
			String mes, String ano, String logradouro, String endereco, String numero, String cidade, String estado,
			String CEP, String cargo) {
		super(prontuario, nome, email, telefone, cpf, rg, dia, mes, ano, logradouro, endereco, numero, cidade, estado, CEP);
		this.setCargo(cargo);
		this.setTipoAssociado(TIPO_ASSOCIADO);
		this.setPrazoEntrega(PRAZO_ENTREGA);
		this.subclassDataObject = new DbQuery_MySQL(this.table, this.campos, this.keyfield);
	}
	
	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	
	public int getTIPO_ASSOCIADO() {
		return TIPO_ASSOCIADO;
	}

	public int getPRAZO_ENTREGA() {
		return PRAZO_ENTREGA;
	}

	public int getQTD_LIVROS_MAX() {
		return QTD_LIVROS_MAX;
	}

	public String[] toArray() {
		return new String[] {
				this.getProntuario(),
				this.getCargo(),
				Integer.toString(this.getPrazoEntrega())
		};
	}

	
}
