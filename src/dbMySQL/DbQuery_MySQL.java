package dbMySQL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbMySQL.ConexaoMySQL;
import dbSQL_Server.ConexaoSQLServer;;

public class DbQuery_MySQL {
	private ConexaoMySQL dbConnection = null;	
	private ConexaoSQLServer db = null;	
	private String tableName;
	private String fields;
	private String keyField;
	
	public DbQuery_MySQL() {
		this.dbConnection = new ConexaoMySQL("localhost", "3306", "biblioteca", "root", "");
		this.db = new ConexaoSQLServer("bdalunos.ifspguarulhos.edu.br", "1433", "ALUNO026", "ALUNO026", "ALUNO026");
	}
	
	public DbQuery_MySQL(String tableName, String fields, String keyField) {
		this.dbConnection = new ConexaoMySQL("localhost", "3306", "biblioteca", "root", "");
		this.db = new ConexaoSQLServer("bdalunos.ifspguarulhos.edu.br", "1433", "ALUNO026", "ALUNO026", "ALUNO026");
		this.setTableName(tableName);
		this.setFields(fields);
		this.setKeyField(keyField);
	}
	
	public ResultSet select(String where) {
		if (this.tableName.isEmpty() || 
			this.fields.isEmpty()){
			System.out.print("Os dados desta classe nao foram informados!");
			return(null);
		}
			
		String sql = ""+ 
					 " SELECT "+
					 this.getFields() +
					 " FROM "+
					 this.getTableName() +
					 (where.isEmpty()?"":(" WHERE "+where));
		System.out.println(sql); // esta aqui por quest�es didaticas
		return (this.executeQuery(sql));
	}
	
	public ResultSet select() {
		return (this.select(""));
	}

	public int insert(String[] values) {	
		String sql = " INSERT INTO "+ 
					 this.getTableName() +
					 " ( "+this.getFields()+") "+
					 "\n VALUES ( ";
		
		for (int i =0; i < values.length; i++){
			sql += "'"+values[i]+"'";
			sql += ((i==values.length-1)?" ) ":", ");
		}
		System.out.println(sql);
		return(this.executeUpdate(sql));
	}
	
	public int insertCurDate(String[] values) {	
		String sql = " INSERT INTO "+ 
					 this.getTableName() +
					 " ( "+this.getFields()+") "+
					 "\n VALUES ( ";
		
		for (int i =0; i < values.length; i++){
			if(values[i].charAt(0) == '!') {
				sql += values[i].substring(1);
			}else {
				sql += "'"+values[i]+"'";	
			}
			sql += ((i==values.length-1)?" ) ":", ");
		}
		System.out.println(sql);
		return(this.executeUpdate(sql));
	}
	
	public int insertFields(String fields, String[] values) {	
		String sql = " INSERT INTO "+ 
					 this.getTableName() +
					 " ("+fields+") "+
					 "\n VALUES ( ";
		
		for (int i =0; i < values.length; i++){
			sql += "'"+values[i]+"'";
			sql += ((i==values.length-1)?" ) ":", ");
		}
		System.out.println(sql);
		return(this.executeUpdate(sql));
	}
	
	public int update(String[] values) {
		String sql = " UPDATE "+ this.getTableName() + " SET ";
		String[] fieldsName = this.fields.split(",");
		int keyFieldIndex = this.getKeyFieldIndex();
		
		for(int i = 0; i< fieldsName.length; i++){
			if (i != keyFieldIndex){
				sql += "\n\t"+fieldsName[i]+" = '"+ values[i] +"'"+
						     ( (i < fieldsName.length-1) ? "," : "" );
			}
		}
		
		sql += "\n WHERE "+ fieldsName[keyFieldIndex]+" = '"+ values[keyFieldIndex]+"'";
		System.out.println(sql);
		return(this.executeUpdate(sql));
	}
	
	public int updateFields(String fields, String[] values) {
		String sql = " UPDATE "+ this.getTableName() + " SET ";
		String[] fieldsName = fields.split(",");
		int keyFieldIndex = this.getKeyFieldIndex();
		
		for(int i = 0; i< fieldsName.length; i++){
			if (i != keyFieldIndex){
				sql += "\n\t"+fieldsName[i]+" = '"+ values[i] +"'"+
						     ( (i < fieldsName.length-1) ? "," : "" );
			}
		}
		
		sql += "\n WHERE "+ fieldsName[keyFieldIndex]+" = '"+ values[keyFieldIndex]+"'";
		System.out.println(sql);
		return(this.executeUpdate(sql));
	}
	
	public int delete(String  keyValue) {
		String sql = " DELETE FROM " + this.tableName +
					 " WHERE "+ keyField +" = '"+keyValue+"'";
		System.out.println(sql);
		return(this.executeUpdate(sql));
	}
	
	public int deleteKey(String key, String  keyValue) {
		String sql = " DELETE FROM " + this.tableName +
					 " WHERE "+ key +" = '"+keyValue+"'";
		System.out.println(sql);
		return(this.executeUpdate(sql));
	}
	
	public int desativa(String keyValue, int SET) {
		String sql = 	"UPDATE " + this.tableName + 
						" SET Ativo = " + SET + 
						" WHERE " + keyField + "= '" + keyValue + "'";
		System.out.println(sql);
		return(this.executeUpdate(sql));
	}
	
	private int getKeyFieldIndex(){
		String[] fieldsName = this.fields.split(",");
		for(int i = 0; i< fieldsName.length; i++){
			if (fieldsName[i].equals(this.keyField)){
				return(i);
			}
		}
		return(0);
	}
	
	public ResultSet executeQuery(String sql) {
		try {
			Statement stmt = this.dbConnection.getStatement();
			ResultSet rs = stmt.executeQuery(sql);
			return(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return(null);
	}
	
	public int executeUpdate(String sql) {
		try {
			Statement stmt = this.dbConnection.getStatement();
			int linesUpdated = stmt.executeUpdate(sql);
			int lines = this.executeUpdateSQLServer(sql);
			return(linesUpdated);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return(0);
	}
	
	public int executeUpdateSQLServer(String sql) {
		try {
			Statement stmt = this.db.getStatement();
			int linesUpdated = stmt.executeUpdate(sql);
			return(linesUpdated);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return(0);
	}

	public ConexaoMySQL getDbConnection() {
		return dbConnection;
	}

	public void setDbConnection(ConexaoMySQL dbConnection) {
		this.dbConnection = dbConnection;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getFields() {
		return fields;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	public String getKeyField() {
		return keyField;
	}

	public void setKeyField(String keyField) {
		this.keyField = keyField;
	}
}
