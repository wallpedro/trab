package xteste;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import view.TelaCadastro;
import view.TelaLogin;
import view.Tela_Inicial;

public class TestTelaInicial extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JPanel 				contentPane;
	private JTextField 			txtPesquisar;
	private JMenuBar 			menuBar; 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestTelaInicial frame = new TestTelaInicial();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestTelaInicial() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 700);
		setLocationRelativeTo(null);
		setTitle("Biblioteca - Instituto Federal de S�o Paulo - Campus Guarulhos");
		setBackground(Color.WHITE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Tela_Inicial.class.getResource("/img/imgBiblioteca.png")));
		setJMenuBar(menuCreate());	

		
		
		contentPane = new JPanel();
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK));
		contentPane.setBackground(Color.WHITE);
		setContentPane(contentPane);
		
		JPanel JPanelPesquisa = new JPanel();
		JPanelPesquisa.setBackground(Color.WHITE);
		JPanelPesquisa.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), " Pesquisa ", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		txtPesquisar = new JTextField();
		txtPesquisar.setToolTipText("Sua pesquisa");
		txtPesquisar.setColumns(10);
		
		JButton btnPesquisar = new JButton("Pesquisar");
		btnPesquisar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnPesquisar.setBackground(Color.WHITE);
		btnPesquisar.setForeground(Color.BLACK);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(8)
					.addComponent(JPanelPesquisa, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(8))
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(8)
					.addComponent(JPanelPesquisa, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 539, Short.MAX_VALUE)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE))
		);
		
		JLabel lblNewLabel = new JLabel("GU3002586");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblUsurio = new JLabel("");
		ImageIcon icon_user = new ImageIcon(Tela_Inicial.class.getResource("/img/icon_user.png"));
		icon_user.setImage(icon_user.getImage().getScaledInstance(16, 16, 100));		
		lblUsurio.setIcon(icon_user);
		lblUsurio.setHorizontalAlignment(SwingConstants.RIGHT);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(872, Short.MAX_VALUE)
					.addComponent(lblUsurio, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
					.addGap(1))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(8)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblUsurio, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		panel.setLayout(gl_panel);
		GroupLayout gl_JPanelPesquisa = new GroupLayout(JPanelPesquisa);
		gl_JPanelPesquisa.setHorizontalGroup(
			gl_JPanelPesquisa.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_JPanelPesquisa.createSequentialGroup()
					.addGap(4)
					.addComponent(txtPesquisar, GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnPesquisar, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
					.addGap(3))
		);
		gl_JPanelPesquisa.setVerticalGroup(
			gl_JPanelPesquisa.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_JPanelPesquisa.createSequentialGroup()
					.addGap(4)
					.addGroup(gl_JPanelPesquisa.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtPesquisar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnPesquisar, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)))
		);
		JPanelPesquisa.setLayout(gl_JPanelPesquisa);
		contentPane.setLayout(gl_contentPane);
		setVisible(true);
	}
	
	
	
	
	private JMenuBar menuCreate() {		
		menuBar = new JMenuBar();		
		menuBar.setBackground(Color.WHITE);
		
		JMenu newMenuAssociado = new JMenu("Associado");
		JMenu newMenuExemplo2 = new JMenu("Exemplo 2");
		
		menuBar.add(newMenuAssociado);
		menuBar.add(newMenuExemplo2);
		
		JMenuItem menuItem = new JMenuItem("Exemplo1");
		newMenuExemplo2.add(menuItem);
		
		JMenuItem menuItem_1 = new JMenuItem("Exemplo2");
		newMenuExemplo2.add(menuItem_1);
		
		JMenuItem menuItem_2 = new JMenuItem("Exemplo3");
		newMenuExemplo2.add(menuItem_2);
		JMenuItem menuItemNovoAssociado = new JMenuItem("Novo Associado");
		menuItemNovoAssociado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new TelaCadastro();
			}
		});
		menuItemNovoAssociado.setHorizontalAlignment(SwingConstants.CENTER);
		newMenuAssociado.add(menuItemNovoAssociado);
		
		JMenuItem menuItemExemplo2 = new JMenuItem("Exemplo2");
		newMenuAssociado.add(menuItemExemplo2);
		
		JMenuItem menuItemExemplo3 = new JMenuItem("Exemplo3");
		newMenuAssociado.add(menuItemExemplo3);
		
		JMenu newMenuExemplo3 = new JMenu("Exemplo 3");
		newMenuExemplo3.setHorizontalAlignment(SwingConstants.LEFT);
		menuBar.add(newMenuExemplo3);
		
		JMenuItem menuItem_3 = new JMenuItem("Exemplo1");
		newMenuExemplo3.add(menuItem_3);
		
		JMenuItem menuItem_4 = new JMenuItem("Exemplo2");
		newMenuExemplo3.add(menuItem_4);
		
		JMenuItem menuItem_5 = new JMenuItem("Exemplo3");
		newMenuExemplo3.add(menuItem_5);
		
		JMenu menuUsuario = new JMenu("Administrador");
		ImageIcon icon_user = new ImageIcon(Tela_Inicial.class.getResource("/img/icon_user.png"));
		icon_user.setImage(icon_user.getImage().getScaledInstance(16, 16, 100));		
		menuUsuario.setIcon(icon_user);
		menuUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(menuUsuario);		
		
		JMenuItem menuItemDeslogar = new JMenuItem("Deslogar");
		menuItemDeslogar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				new TelaLogin();
			}
		});

		menuUsuario.add(menuItemDeslogar);
		menuItemDeslogar.setHorizontalAlignment(SwingConstants.LEFT);
		return (menuBar);
		
	}

}
