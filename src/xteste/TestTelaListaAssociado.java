package xteste;

import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import java.awt.Color;

public class TestTelaListaAssociado extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel 			contentPane;
	private JPanel 			jPanelPesquisa;
	private JButton	 		btnCancelar;
	private JButton			btnBuscar;
	private JTable 			jTableUsuario;
	private JScrollPane		scrollPane;
	private JPanel			jPanelTabela;
	private JLabel 			lblBuscar;
	private JTextField 		txtBuscar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestTelaListaAssociado frame = new TestTelaListaAssociado();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestTelaListaAssociado() {
		getContentPane().setBackground(Color.WHITE);
		setTitle("Tabela de Usu�rio");
		setBounds(50, 50, 1000, 600);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);		
		
		
		jPanelPesquisa = new JPanel();
		jPanelPesquisa.setBackground(Color.WHITE);
		jPanelPesquisa.setBounds(5, 15, 975, 60);
		jPanelPesquisa.setLayout(null);
		
		jPanelTabela 	= new JPanel();
		jPanelTabela.setBackground(Color.WHITE);
		jPanelTabela.setBounds(5, 100, 975, 400);
		
		//Label Buscar
		lblBuscar = new JLabel("Buscar:");
		lblBuscar.setFont(new Font("Arial", Font.PLAIN, 12));
		lblBuscar.setBounds(10,15,45,30);
		//Campo texto Buscar
		txtBuscar = new JTextField();
		txtBuscar.setFont(new Font("Arial", Font.PLAIN, 12));
		txtBuscar.setBounds(60, 15, 795, 30);
		//Bot�o Buscar
		btnBuscar = new JButton("Buscar");
		btnBuscar.setFont(new Font("Arial", Font.BOLD, 12));
		btnBuscar.setBounds(865, 15, 100, 30);		
		//Bot�o Cancelar
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setFont(new Font("Arial", Font.BOLD, 12));
		btnCancelar.setBounds(865, 525, 100, 30);
		
		jPanelPesquisa.add(lblBuscar);
		jPanelPesquisa.add(txtBuscar);
		jPanelPesquisa.add(btnBuscar);
		getContentPane().add(btnCancelar);
		
		getContentPane().setLayout(null);		
		getContentPane().add(jPanelPesquisa);
		getContentPane().add(jPanelTabela);
		
		//jTableUsuario = montarJTable();
		//scrollPane = new JScrollPane(jTableUsuario);
		//jPanelTabela.add(scrollPane);
	}

}
