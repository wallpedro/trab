package dbSQL_Server;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConexaoSQLServer {
	
	private String host;
	private String port;
	private String schema;
	private String userName;
	private String password;
	//private String ignoreTimeZone = "&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private Statement statement= null;
	
	public ConexaoSQLServer(String host, String port, String schema, String userName, String password) {
		this.setHost(host);
		this.setPort(port);
		this.setSchema(schema);
		this.setUserName(userName);
		this.setPassword(password);		

		
		String url = 	"jdbc:sqlserver://"+
						this.getHost()+":"+
						this.getPort()+";databaseName="+
						this.getSchema()+";user="+
						this.getUserName()+";password="+
						this.getPassword()+";";
		System.out.println(url);
		
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
			this.statement = (DriverManager.getConnection(url).createStatement()); 
			if (statement != null)
				System.out.println("Banco de dados Conectado!");
			else
				System.out.println("Banco de dados n�o Conectado!");
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	public Statement getStatement() {
		return( this.statement );
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = ( host == "" ) ? "localhost" : host;
		/*
		if ( host == "" )
			 this.host = "localhost";
		else this.host = host;
		*/
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = ( port == "" ) ? "1433" : port;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = ( userName == "" ) ? "root" : userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = ( password == "" ) ? "" : password;
	}
	/*
	public String getIgnoreTimeZone() {
		return ignoreTimeZone;
	}	
	public void setIgnoreTimeZone(String ignoreTimeZone) {
		this.ignoreTimeZone = ignoreTimeZone;
	}
	*/
	

}
